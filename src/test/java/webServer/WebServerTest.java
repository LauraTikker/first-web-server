package webServer;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import org.junit.Test;

public class WebServerTest {

  private WebServer web = spy(new WebServer(1234));

  @Test
  public void testResponsePrinter()  {
    String message = "Something";

    doReturn(ZonedDateTime.parse("2007-12-03T10:15:30+01:00[Europe/Paris]")).when(web).now();

    String response = web.responsePrinter(message);

    assertEquals("HTTP/1.1 200 OK\n"
        + "Date: 2007-12-03T10:15:30+01:00[Europe/Paris]\n"
        + "Server: Laura Tikker\n"
        + "Content-Length: 9\n"
        + "Content-Type: text.html"  + "\n"
        + "\n"
        + message, response );

  }
  @Test
  public void testRequestPrinter()  {
    ArrayList<String> list = new ArrayList<>();
    list.add("tere");
    list.add("tere2");

    web.requestPrinter(list);

    verify(web).printToConsole("Request:" + "\n");
    verify(web).printToConsole("tere");
    verify(web).printToConsole("tere2");
    verify(web).printToConsole("\n" + "Number of lines in request: " + list.size() + "\n");

  }

  @Test
  public void testStringSplitter()  {
    String longText = "GET /kodumaa HTTP/1.1";

    String response = web.getRequestPath(longText);

    assertEquals("kodumaa", response);
  }


}