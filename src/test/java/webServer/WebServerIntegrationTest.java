package webServer;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Map;
import java.util.function.Function;
import org.junit.Test;

public class WebServerIntegrationTest {


  @Test
  public void testServer() throws UnknownHostException {

    final Runnable startServerRunnable = () -> {
      System.out.println("starting server");
      WebServer web = new WebServer(5656);
      web.addPage("maasikad", "maasikatemaa");
      web.Start();
    };

    final Thread serverThread = new Thread(startServerRunnable);

    serverThread.start();


    try {
      System.out.println("start connecting to server");

      Socket socket = new Socket("localhost", 5656);

      System.out.println("connecting to server");

      OutputStream output = socket.getOutputStream();

      PrintWriter writer = new PrintWriter(output, true);

      writer.println("GET /maasikad HTTP/1.1\n"
          + "User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0\n"
          + "Host: localhost:1234\n"
          + "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\n"
          + "Accept-Language: en-US,en;q=0.5\n"
          + "Accept-Encoding: gzip, deflate\n"
          + "Connection: keep-alive\n"
          + "\n"

      );

      InputStream input = socket.getInputStream();
      BufferedReader reader = new BufferedReader(new InputStreamReader(input));

      ArrayList<String> responseLines = new ArrayList<>();
      String line;

      while ((line = reader.readLine()) != null)   {
        responseLines.add(line);
      }

      String allLines = "";
      for (String lines : responseLines)  {
        allLines += lines;
      }

      socket.close();

      assertEquals("HTTP/1.1 200 OK", responseLines.get(0));
      assertEquals("Content-Length: 12", responseLines.get(3));
      assertEquals("Content-Type: text.html", responseLines.get(4));
      assertEquals("maasikatemaa", responseLines.get(6));
      assertTrue(responseLines.get(1).startsWith("Date: "));


    } catch (Exception e){
      System.out.println("Server exception: " + e.getMessage());
      e.printStackTrace();
    }
  }

  @Test
  public void testPostRequest() throws UnknownHostException {

    Function<Map<String,String>, String> formHandlerForAge = (postBody -> {
      String firstName = postBody.get("firstname");
      String lastName = postBody.get("lastname");
      String age = postBody.get("age");
      return firstName + lastName + age;

    });

    final Runnable startServerRunnable = () -> {
      System.out.println("starting the server");
      WebServer web = new WebServer(2323);
      web.addForm("person", formHandlerForAge);
      web.Start();
    };

    final Thread serverThread = new Thread(startServerRunnable);

    serverThread.start();

    try {
      Socket socket = new Socket("localhost", 2323);
      OutputStream output = socket.getOutputStream();

      PrintWriter writer = new PrintWriter(output, true);

      writer.println("POST /person HTTP/1.1\n"
          + "User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0\n"
          + "Host: localhost:1234\n"
          + "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\n"
          + "Accept-Language: en-US,en;q=0.5\n"
          + "Accept-Encoding: gzip, deflate\n"
          + "Connection: keep-alive\n"
          + "Content-Length: 38\n"
          + "\n"
          + "firstname=Tiina&lastname=Taevas&age=34\n"
          + "\n"

      );

      InputStream input = socket.getInputStream();
      InputStreamReader inputReader = new InputStreamReader(input);
      BufferedReader reader = new BufferedReader(inputReader);

      ArrayList<String> responseLines = new ArrayList<>();

      String line;
      while((line = reader.readLine()) != null) {
        responseLines.add(line);
      }

      assertEquals("HTTP/1.1 200 OK", responseLines.get(0));
      assertTrue(responseLines.get(1).startsWith("Date: "));
      assertEquals("Content-Length: 13", responseLines.get(3));
      assertEquals("Content-Type: text.html", responseLines.get(4));
      assertEquals("", responseLines.get(5));
      assertEquals("TiinaTaevas34", responseLines.get(6));



    } catch (Exception e) {
      System.out.println("Server exception: " + e.getMessage());
      e.printStackTrace();
    }



;  }



}