package webServer;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import org.apache.commons.io.IOUtils;


public class Main {


  public static void main(String[] args) throws URISyntaxException, IOException {

    int portNumber = 2345;

    String jpgFile = "dogs.jpg";
    String gifFile = "cat.gif";
    String pdfFile = "pax.pdf";
    String icon = "sun.jpg";
    String notFound = "giphy.gif";
    String herp = "herp.gif";

    WebServer web = new WebServer(portNumber);

    Function<Map<String, String>, String> formHandler = (postBody -> {
      String firstName = postBody.get("firstname");
      String lastName = postBody.get("lastname");
      return "Tere tulemast koju " + firstName + " " + lastName;
    });

    web.addPage("kodu", "Teretulemast koju");
    web.addPage("mets", "Mine metsa");
    web.addPage("kuusk", "kuusik");
    web.addPage("kontakt", "kontaktandmed");
    web.addPage("abi", "help");
    web.addPage("register", getFileInStrings("register.html"));
    web.addPage("", getFileInStrings("firstPage.html"));



    web.addPage("news", "https://www.postimees.ee/");
    web.addPage("animals", "https://www.tallinnzoo.ee/");
    web.addPage("notFound", getFileInStrings("notFound.html"));

    web.addForm("personaalne-kodu", formHandler);

    web.addHTML("person", "formHandlerForAge.html");



    web.addFile(jpgFile, getFileInBytes(jpgFile));
    web.addFile(gifFile, getFileInBytes(gifFile));
    web.addFile(pdfFile, getFileInBytes(pdfFile));
    web.addFile("favicon.ico", getFileInBytes(icon));
    web.addFile(notFound, getFileInBytes(notFound));
    web.addFile(herp,getFileInBytes(herp));

    web.Start();

  }


  private static byte[] getFileInBytes(String fileName) throws URISyntaxException, IOException {
    return IOUtils.toByteArray(Main.class.getClassLoader().getResourceAsStream(fileName));
  }

  private static String getFileInStrings(String fileName) throws IOException  {

    return IOUtils.toString(Main.class.getClassLoader().getResourceAsStream(fileName), "UTF-8");
  }


}
