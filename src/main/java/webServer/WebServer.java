package webServer;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;


public class WebServer {
  private int portNumber;
  private HashMap<String,String> urlString;
  private HashMap<String,byte[]> urlFile;
  private HashMap<String,Function<Map<String, String>, String>> urlForm;
  private HashMap<String, String> urlHTML;


  public WebServer(int portNumber)  {
    this.portNumber = portNumber;
    this.urlString = new HashMap<>();
    this.urlFile = new HashMap<>();
    this.urlForm = new HashMap<>();
    this.urlHTML = new HashMap<>();
  }


  public void Start() {

    try {
        ServerSocket serverSocket = new ServerSocket(this.portNumber);

        System.out.println("Server is listening to port: " + this.portNumber);

        while (true) {

          Socket socket = serverSocket.accept();

          try {

            System.out.println("Client connected");

            InputStream input = socket.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(input));

            ArrayList<String> requestStrings = requestReader(reader);

            requestPrinter(requestStrings);

            DataOutputStream output = new DataOutputStream(socket.getOutputStream());

            PrintWriter writer = new PrintWriter(output, true);

            responseCheck(requestStrings, writer, output);



          } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
          }

          finally {
            socket.close();
          }

        }

    } catch (Exception e) {

      System.out.println("Server exception: " + e.getMessage());
      e.printStackTrace();
    }
  }

  void responseCheck(ArrayList<String> list, PrintWriter writer, DataOutputStream output) throws IOException {

    String path = getRequestPath(list.get(0));

    if (this.urlForm.containsKey(path)) {

      HashMap<String,String>  bodyInfo = getInfoFromBody(list.get(list.size()-1));

      Function<Map<String, String>, String> formHandler = this.urlForm.get(path);

      final String apply = formHandler.apply(bodyInfo);

      System.out.println(apply);

      writer.println(responsePrinter(apply));

    }
    if (this.urlHTML.containsKey(path)) {

      HashMap<String,String>  bodyInfo = getInfoFromBody(list.get(list.size()-1));

      String template = this.urlHTML.get(path);


      String htmlText = htmlReader(bodyInfo, template);

      writer.println(responsePrinter(htmlText));

    }

    if (this.urlFile.containsKey(path)) {
      responseFilePrinter(output, path);
    }
    if (this.urlString.containsKey(path))  {
      writer.println(responsePrinter(this.urlString.get(path)));
    }
    else {
      writer.println(notFoundPrinter(this.urlString.get("notFound")));
    }
  }

  String htmlReader(HashMap<String,String> map, String template) {


    MustacheFactory mf = new DefaultMustacheFactory();
    StringWriter writerMus = new StringWriter();

    Mustache m = mf.compile(template);
    m.execute(writerMus, map);

    return writerMus.toString();
  }

  String notFoundPrinter(String text)  {
    if (text != null) {
      return "HTTP/1.1 404 Not Found\n"
          + "Date: " + now().toString() + "\n"
          + "Server: Laura Tikker\n"
          + "Content-Length: " + text.length() + "\n"
          + "Content-Type: text.html" + "\n"
          + "\n"
          + text;
    } else  return "";

  }


  String responsePrinter(String text) {

    if (text.contains("www")) {
      String redirect = "HTTP/1.1 301 Moved Permanently\n"
          + "Location: " + text +"\n"
          + "Content-Length: " + text.length() + "\n"
          + "Date: " + now().toString() + "\n"
          + "Content-Type: text.html" + "\n"
          + "\n"
          + text;
      return redirect;
    }
     else  {
      return "HTTP/1.1 200 OK\n"
          + "Date: " + now().toString() + "\n"
          + "Server: Laura Tikker\n"
          + "Content-Length: " + text.length() + "\n"
          + "Content-Type: text.html"  + "\n"
          + "\n"
          + text;
    }
  }

  ZonedDateTime now() {
    return ZonedDateTime.now();
  }

  void responseFilePrinter(DataOutputStream output, String fileName) throws IOException {

    output.writeBytes("HTTP/1.1 200 OK\n");
    output.writeBytes("Date: " + now().toString() + "\n");
    output.writeBytes("Server: Laura Tikker\n");

    if (fileName.endsWith(".jpg"))
      output.writeBytes("Content-Type: image/jpeg\n");
    if (fileName.endsWith(".gif"))
      output.writeBytes("Content-Type: image/gif\n");
    if (fileName.endsWith(".pdf"))
      output.writeBytes("Content/Type: application/pdf\n");

    output.writeBytes("Content-Length: " + this.urlFile.get(fileName).length + "\n");
    output.writeBytes("\n");
    output.write(this.urlFile.get(fileName), 0, this.urlFile.get(fileName).length);

  }

 HashMap<String,String> getInfoFromBody(String text) {

    String[] allParts = text.split("&");
    HashMap<String,String> bodyInfo = new HashMap<>();

    for (String onepart : allParts)  {
      String[] parts = onepart.split("=");
      bodyInfo.put(parts[0], parts[1]);

    }
    return bodyInfo;
  }

    void addForm(String text, Function<Map<String, String>, String> form) {
    if (!this.urlForm.containsKey(text)) {
      this.urlForm.put(text, form);
    }
  }

  public String getRequestPath(String text) {
    if(text != null)  {
      String[] firstPart = text.split(" /");
      String[] secondPart = firstPart[1].split(" ");
      if (secondPart.length == 2) {
        return secondPart[0];
      }
      else return "";

    }
    else return "";

  }

  public String getRequestMethod(String text) {
    if(text != null) {
      String[] firstPart = text.split(" /");
      return firstPart[0];
    }
    else return "";

  }

  public void addPage (String URL, String context) {
    if (!this.urlString.containsKey(URL) && !context.equals(""))  {
      this.urlString.put(URL, context);
    }
  }

  public void addFile(String name, byte[] fileInBytes)  {
    if (!this.urlFile.containsKey(name) && !name.equals(""))  {
      this.urlFile.put(name, fileInBytes);
    }
  }

  public void addHTML(String name, String fileName) {
    if (!this.urlHTML.containsKey(name))  {
      this.urlHTML.put(name, fileName);
    }
  }

  public ArrayList<String> requestReader(BufferedReader reader) {

    ArrayList<String> requestStrings = new ArrayList<>();
    HashMap<String, String> requestHeader = new HashMap<>();

    try {

      String line = reader.readLine();
      requestStrings.add(line);
      String requestedMethod = getRequestMethod(line);

      if (requestedMethod.equals("GET")) {
        while (true) {

          String lineTwo = reader.readLine();
          if (lineTwo.equals("")) {
            break;
          }
          String[] header = headerSplitter(lineTwo);
          if(header.length == 2)  {
            requestHeader.put(header[0], header[1]);
          }
          requestStrings.add(lineTwo);
        }

      }
      if (requestedMethod.equals("POST")) {
        while (true) {

          String lineTwo = reader.readLine();

          if (lineTwo.equals("")) {

            requestStrings.add("");

            char[] requestBody = new char[Integer.parseInt(requestHeader.get("Content-Length"))];

            reader.read(requestBody, 0, Integer.parseInt(requestHeader.get("Content-Length")));

            requestStrings.add(new String(requestBody));

            break;
          }

          String[] header = headerSplitter(lineTwo);
          if(header.length == 2)  {
            requestHeader.put(header[0], header[1]);
          }

          requestStrings.add(lineTwo);
        }
      }

    } catch (Exception e) {
      System.out.println("Reader exception: " + e.getMessage());
      e.printStackTrace();
    }
    return requestStrings;
  }

  private String[] headerSplitter(String text)  {
    String[] stringsSplit = text.split(": ");
    return stringsSplit;
  }


  //seda meetodit ei ole tegelikult vaja, lihtsalt, et n'ha requesti:

  public void requestPrinter(ArrayList<String> list) {

    printToConsole("Request:" + "\n");

    for (String oneLine : list) {
      printToConsole(oneLine);

    }
    printToConsole("\n" + "Number of lines in request: " + list.size() + "\n");
  }

  void printToConsole(String oneLine) {
    System.out.println(oneLine);
  }

}